package com.recycletocoin.Interface;

import android.view.View;

/**
 * Created by Malti on 9/24/2017.
 */

public interface OnRecycleItemClick {
    public void onItemClick(int position, View v);
}
