package com.recycletocoin.utils;

/**
 * Created by sandy on 13-08-2017.
 */

public interface QrListener {
    void getCode(String code);
}
