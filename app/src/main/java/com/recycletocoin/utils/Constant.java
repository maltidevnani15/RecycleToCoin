package com.recycletocoin.utils;

public class Constant {
    public static final String KEY_STATUS="res_code";
    public static final String KEY_MESSAGE="res_msg";
    public static final String KEY_DATA = "data";

    public static final int REQUEST_CHECK_SETTINGS = 1111;
    public static final String WALK_THROUGH="walthrough";
    public  static  final String LOGIN_DONE="";

    public static final String USER_DATA="user_data";
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
}
