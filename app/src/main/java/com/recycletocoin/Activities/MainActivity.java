package com.recycletocoin.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.recycletocoin.Model.BalanceModel;
import com.recycletocoin.Model.UserModel;
import com.recycletocoin.R;
import com.recycletocoin.RecycleToCoinApplication;

import com.recycletocoin.databinding.ActivityMaineBinding;
import com.recycletocoin.utils.Constant;
import com.recycletocoin.utils.Logger;
import com.recycletocoin.utils.Utils;
import com.recycletocoin.webServices.RestClient;
import com.recycletocoin.webServices.RetrofitCallback;

import retrofit2.Call;


public class MainActivity extends BaseActivity implements View.OnClickListener {
    ActivityMaineBinding activityMainBinding;
    public static MainActivity instance;


    @Override
    protected void initView() {
        instance = this;
        checkLocationPermission();

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_maine);
        activityMainBinding.activityMainTvAppLink.setMovementMethod(LinkMovementMethod.getInstance());
        activityMainBinding.activityMainLlClaimToken.setOnClickListener(this);
        activityMainBinding.activityMainLlTransaction.setOnClickListener(this);
        activityMainBinding.activityMaineLlProfile.setOnClickListener(this);
        activityMainBinding.activityMaineLlSearchStore.setOnClickListener(this);
        activityMainBinding.activityMaineLlHelp.setOnClickListener(this);


    }


    @Override
    protected void initToolbar() {

    }

    private void checkLocationPermission() {
        final int finePermissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        final int coarsePermissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        if (finePermissionCheck != PackageManager.PERMISSION_GRANTED && coarsePermissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        10025);
            }
        } else {
            final double latitude = RecycleToCoinApplication.getInstance().getLatitude();
            final double longitude = RecycleToCoinApplication.getInstance().getLongitude();
            if (latitude == 0 && longitude == 0) {
                showLocationEnableDialog();
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        RecycleToCoinApplication.getInstance().stopLocationUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();
        RecycleToCoinApplication.getInstance().startLocationUpdates();
        checkBalance();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 10025: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    final double latitude = RecycleToCoinApplication.getInstance().getLatitude();
                    final double longitude = RecycleToCoinApplication.getInstance().getLongitude();
                    if (latitude == 0 && longitude == 0) {
                        showLocationEnableDialog();
                    }
                } else {
                    Logger.toast(this, "Please enable location from Apps setting");

                }
                break;
            }

        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.activity_main_ll_claim_token:
                intent = new Intent(this, MyScanActivity.class);
                intent.putExtra("FromClaimToken", true);
                navigateToNextActivity(intent, false);
                break;
            case R.id.activity_maine_ll_search_store:
                intent = new Intent(this, LocateStoreActivity.class);
                navigateToNextActivity(intent, false);
                break;
            case R.id.activity_main_ll_transaction:
                intent = new Intent(this, TransactionActivity.class);
                navigateToNextActivity(intent, false);
                break;
            case R.id.activity_maine_ll_profile:
                intent = new Intent(this, ProfileActivity.class);
                navigateToNextActivity(intent, false);
                break;
            case R.id.activity_maine_ll_help:
                String url = "http://recycletocoin.com/app/page/index/help";
                intent = new Intent(this, WebViewActivity.class);
                intent.putExtra("url", url);
                navigateToNextActivity(intent, false);
             break;
          }
    }

    private void checkBalance() {
        String userData=Utils.getString(this,Constant.USER_DATA);
        UserModel userModel = new Gson().fromJson(userData,UserModel.class);
        Log.e("user_id in check",userModel.getUserId());
        Call<BalanceModel> checkBal = RestClient.getInstance().getApiInterface().checkBalance(userModel.getUserId());
        checkBal.enqueue(new RetrofitCallback<BalanceModel>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(BalanceModel data) {
                activityMainBinding.activityMaineTvAvailablePoints.setText(data.getAvailable_points());
                activityMainBinding.activityMainTvTotalpoints.setText(data.getTotal_points());
                activityMainBinding.activityMaineTvRedeemepoints.setText(data.getRedeemed_points());


            }

            @Override
            public void onFailure(Call<BalanceModel> call, Throwable error) {
                super.onFailure(call, error);
                onBackPressed();
            }
        });
    }

    public void showLocationEnableDialog() {
        final LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(RecycleToCoinApplication.getInstance().getLocationRequest());
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(RecycleToCoinApplication.getInstance().getGoogleApiClient(), builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        RecycleToCoinApplication.getInstance().onConnected(null);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(MainActivity.this, Constant.REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    public static MainActivity getInstance() {
        return instance;
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }
    }
}
