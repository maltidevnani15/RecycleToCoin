package com.recycletocoin.Activities;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.recycletocoin.Adapter.TransactionAdapter;
import com.recycletocoin.Model.TransactionModel;
import com.recycletocoin.Model.UserModel;
import com.recycletocoin.R;
import com.recycletocoin.databinding.ActivityTransactionBinding;
import com.recycletocoin.utils.Constant;
import com.recycletocoin.utils.Logger;
import com.recycletocoin.utils.Utils;
import com.recycletocoin.webServices.RestClient;
import com.recycletocoin.webServices.RetrofitCallback;

import java.util.ArrayList;

import retrofit2.Call;

public class TransactionActivity extends BaseActivity implements View.OnClickListener {
    ActivityTransactionBinding activityTransactionBinding;
    private TransactionAdapter transactionAdapter;

    private ArrayList<TransactionModel>transactionModelArrayList;

    @Override
    protected void initView() {

        transactionModelArrayList=new ArrayList<>();
        activityTransactionBinding= DataBindingUtil.setContentView(this,R.layout.activity_transaction);
        activityTransactionBinding.activityTransactionRv.setLayoutManager(new LinearLayoutManager(this));
        transactionAdapter=new TransactionAdapter(this,transactionModelArrayList);
        activityTransactionBinding.activityTransactionRv.setAdapter(transactionAdapter);
        getTransaction();

    }

    private void getTransaction() {
        String userData=Utils.getString(this,Constant.USER_DATA);
        UserModel userModel = new Gson().fromJson(userData,UserModel.class);
        Call<ArrayList<TransactionModel>>getTransaction= RestClient.getInstance().getApiInterface().getTransaction(userModel.getUserId());
        getTransaction.enqueue(new RetrofitCallback<ArrayList<TransactionModel>>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ArrayList<TransactionModel> data) {
                if(!data.isEmpty()){
                    activityTransactionBinding.activityTransactionTvNoData.setVisibility(View.GONE);
                    transactionModelArrayList.trimToSize();
                    transactionModelArrayList.addAll(data);
                    transactionAdapter.notifyDataSetChanged();

                }else{
                    activityTransactionBinding.activityTransactionTvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<TransactionModel>> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }

    @Override
    protected void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_transaction_tb);
        toolbar.setBackgroundColor(getResources().getColor(R.color.purple));
        final ImageView logo=(ImageView)toolbar.findViewById(R.id.row_tb_iv_logo);
        final ImageView terms=(ImageView)toolbar.findViewById(R.id.row_tb_iv_terms);
        final ImageView help=(ImageView)toolbar.findViewById(R.id.row_tb_iv_help);
        final ImageView back=(ImageView)toolbar.findViewById(R.id.row_tb_iv_back);
        logo.setImageResource(R.drawable.ic_logo_white);
        back.setVisibility(View.VISIBLE);
        terms.setVisibility(View.GONE);
        help.setVisibility(View.GONE);
        back.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.row_tb_iv_back:
                onBackPressed();
                break;

        }
    }
}
