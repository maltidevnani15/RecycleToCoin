package com.recycletocoin.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.recycletocoin.Model.BalanceModel;
import com.recycletocoin.Model.UserModel;
import com.recycletocoin.R;
import com.recycletocoin.databinding.ActivityScanBinding;
import com.recycletocoin.utils.Constant;
import com.recycletocoin.utils.Logger;
import com.recycletocoin.utils.Utils;
import com.recycletocoin.webServices.RestClient;
import com.recycletocoin.webServices.RetrofitCallback;

import java.io.IOException;

import retrofit2.Call;

public class MyScanActivity extends BaseActivity implements View.OnClickListener, SurfaceHolder.Callback {
    private ActivityScanBinding activityScanBinding;
    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;

    private boolean isScanComplete = false;
    private String data;
    @Override
    protected void initView() {
        Intent i=getIntent();

        activityScanBinding = DataBindingUtil.setContentView(this, R.layout.activity_scan);

        activityScanBinding.activityScanTvStop.setOnClickListener(this);
    }

    @Override
    protected void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_scan_tb);
        toolbar.setBackgroundColor(getResources().getColor(R.color.purple));
        final ImageView logo=(ImageView)toolbar.findViewById(R.id.row_tb_iv_logo);
        final ImageView terms=(ImageView)toolbar.findViewById(R.id.row_tb_iv_terms);
        final ImageView help=(ImageView)toolbar.findViewById(R.id.row_tb_iv_help);
        final ImageView back=(ImageView)toolbar.findViewById(R.id.row_tb_iv_back);
        logo.setImageResource(R.drawable.ic_logo_white);
        back.setVisibility(View.VISIBLE);
        terms.setVisibility(View.GONE);
        help.setVisibility(View.GONE);
        back.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        barcodeDetector =
                new BarcodeDetector.Builder(this)
                        .setBarcodeFormats(Barcode.DATA_MATRIX |Barcode.QR_CODE)
                        .build();

        cameraSource = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(700, 700)
                .setAutoFocusEnabled(true)
                .build();
        activityScanBinding.activityScanSurfaceView.getHolder().addCallback(this);

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if(barcodes.size()!=0){
                    data= barcodes.valueAt(0).displayValue;
                    if (!TextUtils.isEmpty(data) && !isScanComplete){
                        isScanComplete = true;
                        activityScanBinding.activityScanTvCodeGet.post(new Runnable() {    // Use the post method of the TextView
                            public void run() {
                                activityScanBinding.activityScanTvCodeGet.setText(    // Update the TextView
                                        barcodes.valueAt(0).displayValue
                                );
                                data = barcodes.valueAt(0).displayValue;
                                getData();

                            }
                        });
                    }
                }

                 }
        });
    }

    private void getData() {


            claimToken();


    }

    private void claimToken() {
        if (!TextUtils.isEmpty(data)) {
            String userData = Utils.getString(this, Constant.USER_DATA);
            if (!TextUtils.isEmpty(userData)) {
                UserModel userModel = new Gson().fromJson(userData, UserModel.class);
                Call<JsonObject> claimToken = RestClient.getInstance().getApiInterface().claimToken(data,userModel.getUserId());
                claimToken.enqueue(new RetrofitCallback<JsonObject>(this, Logger.showProgressDialog(this)) {
                    @Override
                    public void onSuccess(JsonObject data) {
                        if (data.get("res_code").getAsInt() == 0) {
                            moveToSuccessScreen(data.get(Constant.KEY_MESSAGE).getAsString());
                        } else {
                            Logger.toast(MyScanActivity.this, data.get("res_msg").getAsString());
                            onBackPressed();
                        }
                    }
                });
            } else {
                Logger.showSnackbar(this, "Scan not properly");
            }


        }
    }
    private void moveToSuccessScreen(String sucessMessage) {

        Intent i = new Intent(this, SuccessActivity.class);
        i.putExtra("successMessage",sucessMessage);
        navigateToNextActivity(i, true);
    }


     @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_scan_tv_stop:
                onBackPressed();
                break;
            case R.id.row_tb_iv_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if (ActivityCompat.checkSelfPermission(MyScanActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            cameraSource.start(activityScanBinding.activityScanSurfaceView.getHolder());
        } catch (IOException ie) {
            Log.e("CAMERA SOURCE", ie.getMessage());
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        cameraSource.stop();
    }
}
