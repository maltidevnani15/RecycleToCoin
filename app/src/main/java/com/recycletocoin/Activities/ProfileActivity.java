package com.recycletocoin.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.recycletocoin.Model.UserModel;
import com.recycletocoin.R;
import com.recycletocoin.databinding.ActivityProfileBinding;
import com.recycletocoin.utils.Constant;
import com.recycletocoin.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileActivity extends BaseActivity implements View.OnClickListener {
    ActivityProfileBinding activityProfileBinding;
    JSONObject jsonObject;

    @Override
    protected void initView() {
      activityProfileBinding= DataBindingUtil.setContentView(ProfileActivity.this,R.layout.activity_profile);
        String userData=Utils.getString(this,Constant.USER_DATA);
        if(!TextUtils.isEmpty(userData)){
            UserModel userModel = new Gson().fromJson(userData, UserModel.class);
            activityProfileBinding.activityProfileTvName.setText(userModel.getFullName());
            activityProfileBinding.activityProfileTvUserName.setText(userModel.getFullName().toUpperCase());
            activityProfileBinding.activityProfileTvEmail.setText(userModel.getEmail());
        }
        activityProfileBinding.activityProfileTvLogout.setOnClickListener(this);

    }

    @Override
    protected void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_profile_tb);
        toolbar.setBackgroundColor(getResources().getColor(R.color.purple));
        final ImageView logo=(ImageView)toolbar.findViewById(R.id.row_tb_iv_logo);
        final ImageView terms=(ImageView)toolbar.findViewById(R.id.row_tb_iv_terms);
        final ImageView help=(ImageView)toolbar.findViewById(R.id.row_tb_iv_help);
        final ImageView back=(ImageView)toolbar.findViewById(R.id.row_tb_iv_back);
        logo.setImageResource(R.drawable.ic_logo_white);
        back.setVisibility(View.VISIBLE);
        terms.setVisibility(View.GONE);
        help.setVisibility(View.GONE);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.row_tb_iv_back:
                onBackPressed();
                break;
            case R.id.activity_profile_tv_logout:
                Utils.storeString(this, Constant.USER_DATA,"");
                Utils.storeString(ProfileActivity.this,Constant.WALK_THROUGH,"done");
                Log.e("in logoutId",Utils.getString(this,Constant.USER_DATA));
                Log.e("in logout",Utils.getString(this,Constant.WALK_THROUGH));

                Intent i = new Intent(this,LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                navigateToNextActivity(i,true);
                break;
        }

    }
}
