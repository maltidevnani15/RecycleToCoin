package com.recycletocoin.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.google.gson.JsonObject;
import com.recycletocoin.R;
import com.recycletocoin.databinding.ActivitySignUpBinding;
import com.recycletocoin.utils.Constant;
import com.recycletocoin.utils.Logger;
import com.recycletocoin.utils.Utils;
import com.recycletocoin.webServices.RestClient;
import com.recycletocoin.webServices.RetrofitCallback;

import retrofit2.Call;

public class SignUpActivity extends BaseActivity implements View.OnClickListener {
    private ActivitySignUpBinding activitySignUpBinding;
    @Override
    protected void initView() {
     activitySignUpBinding= DataBindingUtil.setContentView(this,R.layout.activity_sign_up);
        activitySignUpBinding.activitySignupTvSugnup.setOnClickListener(this);
        activitySignUpBinding.activitySignUpTvTerms.setOnClickListener(this);

    }

    @Override
    protected void initToolbar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.activity_signup_tb);
        toolbar.setBackgroundColor(getResources().getColor(R.color.purple));
        final ImageView logo=(ImageView)toolbar.findViewById(R.id.row_tb_iv_logo);
        final ImageView terms=(ImageView)toolbar.findViewById(R.id.row_tb_iv_terms);
        final ImageView help=(ImageView)toolbar.findViewById(R.id.row_tb_iv_help);
        final ImageView back=(ImageView)toolbar.findViewById(R.id.row_tb_iv_back);
        logo.setImageResource(R.drawable.ic_logo_white);
        back.setVisibility(View.VISIBLE);
        terms.setVisibility(View.GONE);
        help.setVisibility(View.GONE);
        back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_signup_tv_sugnup:
                validateData();
                break;
            case R.id.row_tb_iv_back:
                onBackPressed();
                break;

            case R.id.activity_signUp_tv_terms:
                String termsUrl = "http://recycletocoin.com/app/page/index/terms_and_conditions";
               Intent intent = new Intent(this, WebViewActivity.class);
                intent.putExtra("url", termsUrl);
                navigateToNextActivity(intent, false);
                break;
        }
    }

    private void validateData() {
        final String fullname=activitySignUpBinding.activitySignupEtFullname.getText().toString();
        final String email=activitySignUpBinding.activitySignupEtEmail.getText().toString();
        final String password=activitySignUpBinding.activitySignupEtPwd.getText().toString();
        final String repeatPwd=activitySignUpBinding.activitySignupEtRepwd.getText().toString();
        if(!TextUtils.isEmpty(fullname)){
            if(!TextUtils.isEmpty(email)){
                if(Utils.isEmailValid(email)){
                    if(!TextUtils.isEmpty(password)){
                        if(!TextUtils.isEmpty(repeatPwd)){
                            if(password.matches(repeatPwd)){
                                if(activitySignUpBinding.activitySignUpCbTerms.isChecked()){
                                    doSignUp(fullname,email,password);
                                }else{
                                    Logger.showSnackbar(this,"Terms and Condition is not checked");
                                }

                            }else {
                                Logger.showSnackbar(this,"Repeat password and password does not match");
                            }
                        }else{
                            Logger.showSnackbar(this,"Repeat password is empty");
                        }
                    }else{
                        Logger.showSnackbar(this,"password is empty");
                    }
                }else{
                    Logger.showSnackbar(this,"email is not valid");
                }

            }else {
                Logger.showSnackbar(this,"email is empty");
            }
        }else{
            Logger.showSnackbar(this,"Full name is empty");
        }

    }

    private void doSignUp(String fullname, String email, String password) {
        Call<JsonObject>doSignup=new RestClient().getApiInterface().signUp(fullname,email,password);
        doSignup.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(SignUpActivity.this)) {
            @Override
            public void onSuccess(JsonObject data) {
               if(data.get(Constant.KEY_STATUS).getAsInt()==0){
                   moveToNextActivity();
               }else{
                   Logger.showSnackbar(SignUpActivity.this,data.get(Constant.KEY_MESSAGE).getAsString());
               }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }

    private void moveToNextActivity() {
        Logger.toast(this,"Kindly Login now");
        Intent i = new Intent(this,LoginActivity.class);
        navigateToNextActivity(i,true);
    }


}
