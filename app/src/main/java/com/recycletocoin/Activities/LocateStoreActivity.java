package com.recycletocoin.Activities;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.recycletocoin.Adapter.StoresAdapter;
import com.recycletocoin.Interface.OnRecycleItemClick;
import com.recycletocoin.Model.StoreModel;
import com.recycletocoin.R;
import com.recycletocoin.RecycleToCoinApplication;
import com.recycletocoin.databinding.ActivityLocateStoreBinding;
import com.recycletocoin.utils.Logger;
import com.recycletocoin.webServices.RestClient;
import com.recycletocoin.webServices.RetrofitCallback;

import java.util.ArrayList;

import retrofit2.Call;

import static com.recycletocoin.R.id.map;

public class LocateStoreActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, OnRecycleItemClick, View.OnClickListener {
    private ActivityLocateStoreBinding activityLocateStoreBinding;
    private GoogleMap myMap;
    private ArrayList<StoreModel>storeModelArrayList;
    private StoresAdapter storesAdapter;

    @Override
    protected void initView() {
        storeModelArrayList=new ArrayList<>();
        activityLocateStoreBinding= DataBindingUtil.setContentView(this,R.layout.activity_locate_store);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);
        activityLocateStoreBinding.activityLocateStoreRv.setLayoutManager(new LinearLayoutManager(this));
        storesAdapter=new StoresAdapter(this,storeModelArrayList);
        storesAdapter.setOnRecycleItemClick(this);
        activityLocateStoreBinding.activityLocateStoreRv.setAdapter(storesAdapter);

        getStores();
    }

    @Override
    protected void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_locate_store_tb);
        toolbar.setBackgroundColor(getResources().getColor(R.color.purple));
        final ImageView logo=(ImageView)toolbar.findViewById(R.id.row_tb_iv_logo);
        final ImageView terms=(ImageView)toolbar.findViewById(R.id.row_tb_iv_terms);
        final ImageView help=(ImageView)toolbar.findViewById(R.id.row_tb_iv_help);
        final ImageView back=(ImageView)toolbar.findViewById(R.id.row_tb_iv_back);
        logo.setImageResource(R.drawable.ic_logo_white);
        back.setVisibility(View.VISIBLE);
        terms.setVisibility(View.GONE);
        help.setVisibility(View.GONE);
        back.setOnClickListener(this);

    }
    private void getStores() {
        final double latitude= RecycleToCoinApplication.getInstance().getLatitude();
        final double longitude=RecycleToCoinApplication.getInstance().getLongitude();
        Log.e("latitude", String.valueOf(latitude));
        if(latitude==0.0 && longitude==0.0){
            MainActivity.getInstance().showLocationEnableDialog();
        }else{
            Call<ArrayList<StoreModel>> getStore= RestClient.getInstance().getApiInterface().getStores(String.valueOf(latitude),String.valueOf(longitude));
            getStore.enqueue(new RetrofitCallback<ArrayList<StoreModel>>(this, Logger.showProgressDialog(this)) {
                @Override
                public void onSuccess(ArrayList<StoreModel> data) {
                    if(!data.isEmpty()){
                        storeModelArrayList.trimToSize();
                        storeModelArrayList.addAll(data);
                        storesAdapter.notifyDataSetChanged();
                        onMapLoaded();

                        Log.e("storeLat",data.get(0).getFullname());
                    }

                }

                @Override
                public void onFailure(Call<ArrayList<StoreModel>> call, Throwable error) {
                    super.onFailure(call, error);
                }
            });
        }

    }
     @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        myMap=googleMap;
        myMap.setOnMapLoadedCallback(this);

    }

    @Override
    public void onMapLoaded() {
        if(!storeModelArrayList.isEmpty()){
            for(int i = 0 ; i < storeModelArrayList.size() ; i++ ) {
                createMarker(storeModelArrayList.get(i).getLatitude(), storeModelArrayList.get(i).getLongitude());
            }
        }

    }
    protected void createMarker(String latitude, String longitude) {
        final double lat=Double.parseDouble(latitude);
        final double lang=Double.parseDouble(longitude);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(new LatLng(lat, lang));
        myMap.addMarker(new MarkerOptions().position(new LatLng(lat, lang)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker)));
        LatLngBounds bounds = builder.build();
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,100 );
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        myMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 15));
        myMap.moveCamera(cu);
        myMap.animateCamera(zoom);

    }

    @Override
    public void onItemClick(int position, View v) {
        if(isGoogleMapsInstalled()){
            String url = "http://maps.google.com/maps?saddr="+RecycleToCoinApplication.getInstance().getLatitude()+","+RecycleToCoinApplication.getInstance().getLongitude()+
                    "&daddr="+storeModelArrayList.get(position).getLatitude()+","+storeModelArrayList.get(position).getLongitude()+"&mode=driving";
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            startActivity(intent);
        }else{
            Logger.toast(this,"Kindly install google map");
        }

    }
    public boolean isGoogleMapsInstalled()
    {
        try
        {
            ApplicationInfo info = getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0 );
            return true;
        }
        catch(PackageManager.NameNotFoundException e)
        {
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.row_tb_iv_back:
                onBackPressed();
                break;
        }

    }
}
