package com.recycletocoin.Activities;

import android.content.Intent;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.recycletocoin.Model.UserModel;
import com.recycletocoin.R;
import com.recycletocoin.utils.Constant;
import com.recycletocoin.utils.Logger;
import com.recycletocoin.utils.Utils;

public class SplashActivity extends BaseActivity {

    @Override
    protected void initView() {
        setContentView(R.layout.activity_splash);
        new SplashCountDown(2000,1000).start();
    }

    @Override
    protected void initToolbar() {

    }


    private class SplashCountDown extends CountDownTimer {
        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public SplashCountDown(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {

           if(Utils.isConnectedToInternet(SplashActivity.this)){
               UserModel userModel = null;
               String walkthroughDone=Utils.getString(SplashActivity.this,Constant.WALK_THROUGH);
               String userData=Utils.getString(SplashActivity.this,Constant.USER_DATA);
               if(!TextUtils.isEmpty(userData)){
                  userModel = new Gson().fromJson(userData,UserModel.class);

               }
              
               Log.e("walkin Splash",walkthroughDone);
               Log.e("waldata",userData);
               if(TextUtils.isEmpty(walkthroughDone)){
                   Intent intent = new Intent(SplashActivity.this, WalkThroughActivity.class);
                   navigateToNextActivity(intent, true);
               }else if(userModel !=null){
                   Log.e("userIdin splash",userModel.getUserId());
                   Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                   navigateToNextActivity(intent, true);
               }else{

                   Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                   navigateToNextActivity(intent, true);
               }

           }else{
               Logger.toast(SplashActivity.this,getString(R.string.not_internet_connection));
               finish();
           }

        }

    }

    @Override
    protected void onStart() {
        super.onStart();


    }
}
