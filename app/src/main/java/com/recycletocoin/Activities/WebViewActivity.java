package com.recycletocoin.Activities;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.recycletocoin.R;
import com.recycletocoin.databinding.ActivityWebViewBinding;
import com.recycletocoin.utils.Logger;

public class WebViewActivity extends BaseActivity implements View.OnClickListener {
    private ActivityWebViewBinding activityWebViewBinding;

    @Override
    protected void initView() {
        activityWebViewBinding = DataBindingUtil.setContentView(this, R.layout.activity_web_view);
        startWebView(getIntent().getStringExtra("url"));
    }

    private void startWebView(String s) {
        activityWebViewBinding.activityMainWebView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressDialog = Logger.showProgressDialog(WebViewActivity.this);
            }

            public void onPageFinished(WebView view, String url) {
                Logger.dismissProgressDialog(progressDialog);

            }
        });
        activityWebViewBinding.activityMainWebView.getSettings().setJavaScriptEnabled(true);
        activityWebViewBinding.activityMainWebView.loadUrl(s);
    }


    @Override
    protected void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_webView_tb);
        toolbar.setBackgroundColor(getResources().getColor(R.color.purple));
        final ImageView logo = (ImageView) toolbar.findViewById(R.id.row_tb_iv_logo);
        final ImageView terms = (ImageView) toolbar.findViewById(R.id.row_tb_iv_terms);
        final ImageView help = (ImageView) toolbar.findViewById(R.id.row_tb_iv_help);
        final ImageView back = (ImageView) toolbar.findViewById(R.id.row_tb_iv_back);
        logo.setImageResource(R.drawable.ic_logo_white);
        back.setVisibility(View.VISIBLE);
        terms.setVisibility(View.GONE);
        help.setVisibility(View.GONE);
        back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.row_tb_iv_back:
                onBackPressed();
                break;

        }
    }


}
