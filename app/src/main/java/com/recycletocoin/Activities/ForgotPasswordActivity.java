package com.recycletocoin.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.JsonObject;
import com.recycletocoin.R;
import com.recycletocoin.databinding.ActivityForgotPasswordBinding;
import com.recycletocoin.utils.Constant;
import com.recycletocoin.utils.Logger;
import com.recycletocoin.utils.Utils;
import com.recycletocoin.webServices.RestClient;
import com.recycletocoin.webServices.RetrofitCallback;

import retrofit2.Call;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {
    private ActivityForgotPasswordBinding activityForgotPasswordBinding;


    @Override
    protected void initView() {
        activityForgotPasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        activityForgotPasswordBinding.activityForgotPwdTvSend.setOnClickListener(this);
    }

    @Override
    protected void initToolbar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.activity_forgot_pwd_tb);
        final ImageView logo = (ImageView) toolbar.findViewById(R.id.row_tb_iv_logo);
        final ImageView terms = (ImageView) toolbar.findViewById(R.id.row_tb_iv_terms);
        final ImageView help = (ImageView) toolbar.findViewById(R.id.row_tb_iv_help);
        final  ImageView backbt=(ImageView)toolbar.findViewById(R.id.row_tb_iv_back);

        logo.setImageResource(R.drawable.ic_logo_white);
        backbt.setVisibility(View.VISIBLE);
        terms.setVisibility(View.GONE);
        help.setVisibility(View.GONE);
        backbt.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_forgot_pwd_tv_send:
                validateData();
                break;
            case R.id.row_tb_iv_back:
                onBackPressed();
                break;
        }
    }

    private void validateData() {
        final String email=activityForgotPasswordBinding.activityForgotPwdEtEmail.getText().toString();
        if(!TextUtils.isEmpty(email) && Utils.isEmailValid(email)){
            sendResetLink(email);
        }else{
            Logger.showSnackbar(this,"Email is blank or not valid");
        }
    }

    private void sendResetLink(String email) {
        Call<JsonObject>sendMail= RestClient.getInstance().getApiInterface().sendEmail(email);
        sendMail.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data)
            {
                Logger.toast(ForgotPasswordActivity.this,data.get(Constant.KEY_MESSAGE).getAsString());
               onBackPressed();
                  }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }


}
