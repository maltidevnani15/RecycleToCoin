package com.recycletocoin.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.recycletocoin.Model.UserModel;
import com.recycletocoin.R;
import com.recycletocoin.databinding.ActivityLoginBinding;
import com.recycletocoin.utils.Constant;
import com.recycletocoin.utils.Logger;
import com.recycletocoin.utils.Utils;
import com.recycletocoin.webServices.RestClient;
import com.recycletocoin.webServices.RetrofitCallback;

import retrofit2.Call;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private ActivityLoginBinding loginActivity;

    @Override
    protected void initView() {
        loginActivity = DataBindingUtil.setContentView(this, R.layout.activity_login);
        loginActivity.activityLoginTvLogin.setOnClickListener(this);
        loginActivity.activityLoginTvForgotPwd.setOnClickListener(this);
        loginActivity.activityLoginTvSignUp.setOnClickListener(this);


    }

    @Override
    protected void initToolbar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.activity_login_tb);
        final ImageView logo = (ImageView) toolbar.findViewById(R.id.row_tb_iv_logo);
        final ImageView terms = (ImageView) toolbar.findViewById(R.id.row_tb_iv_terms);
        final ImageView help = (ImageView) toolbar.findViewById(R.id.row_tb_iv_help);

        logo.setImageResource(R.drawable.ic_logo_white);

        terms.setVisibility(View.GONE);
        help.setVisibility(View.GONE);

    }

   @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_login_tv_forgot_pwd:
                Intent intent =new Intent(this,ForgotPasswordActivity.class);
                navigateToNextActivity(intent,false);
                break;
            case R.id.activity_login_tv_login:
                validateData();
                break;
            case R.id.activity_login_tv_sign_up:
                Intent i =new Intent(this,SignUpActivity.class);
                navigateToNextActivity(i,false);
                break;
        }
    }

    private void validateData() {
        final String email=loginActivity.activityLoginEtUsername.getText().toString();
        final String pwd=loginActivity.activityLoginEtPwd.getText().toString();
        if(!TextUtils.isEmpty(email)){
            if(!TextUtils.isEmpty(pwd)){
               dologin(email,pwd);
            }else{
                Logger.showSnackbar(this,"Password is empty");
            }
        }else{
            Logger.showSnackbar(this,"Email is empty");
        }
    }

    private void dologin(String email, String pwd) {
        Call<UserModel>dologin= RestClient.getInstance().getApiInterface().doLogin(email,pwd);
        dologin.enqueue(new RetrofitCallback<UserModel>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(UserModel data) {
                Utils.storeString(LoginActivity.this,Constant.USER_DATA,new Gson().toJson(data));
                Intent i = new Intent(LoginActivity.this,MainActivity.class);
                Logger.toast(LoginActivity.this,"Welcome");
                navigateToNextActivity(i,true);
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }

    private void moveToNextScreen(JsonObject data) {


    }

}
