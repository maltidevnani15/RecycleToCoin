package com.recycletocoin.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;


import com.recycletocoin.R;


public abstract class BaseActivity extends AppCompatActivity {
      protected abstract void initView();
        protected abstract void initToolbar();



        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            initView();
            initToolbar();
        }
        public void navigateToNextActivity(Intent intent, boolean isFinish) {
            startActivity(intent);
            if (isFinish)
                finish();
            overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
        }


}
