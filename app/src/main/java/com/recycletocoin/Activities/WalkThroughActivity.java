package com.recycletocoin.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.recycletocoin.Adapter.WalkThroughAdapter;
import com.recycletocoin.R;
import com.recycletocoin.databinding.ActivityWalkThroughBinding;
import com.recycletocoin.utils.Constant;
import com.recycletocoin.utils.Utils;

public class WalkThroughActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    private ActivityWalkThroughBinding activityWalkThroughBinding;
    private Context context;
    private String[] image_text_array_heading;
    private String[] image_text_array_content;
    int[] mResources = {
            R.drawable.slide_1,
            R.drawable.slide_2,
            R.drawable.slide_3,
            R.drawable.slide_4,

    };
    private WalkThroughAdapter walkThroughAdapter;

    @Override
    protected void initView() {
        context=WalkThroughActivity.this;
        activityWalkThroughBinding= DataBindingUtil.setContentView(this,R.layout.activity_walk_through);
        Window window=WalkThroughActivity.this.getWindow();
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            window.setStatusBarColor(WalkThroughActivity.this.getResources().getColor(R.color.purple));

        }
        image_text_array_heading = context.getResources().getStringArray(R.array.walkthrough_image_text_heading);
        image_text_array_content = context.getResources().getStringArray(R.array.walkthrough_image_text);

        walkThroughAdapter=new WalkThroughAdapter(this,mResources);
        activityWalkThroughBinding.activityWalkThroughVp.setAdapter(walkThroughAdapter);
        activityWalkThroughBinding.activityWalkThroughCp.setViewPager(activityWalkThroughBinding.activityWalkThroughVp);
        activityWalkThroughBinding.activityWalkThroughCp.setOnPageChangeListener(this);
        activityWalkThroughBinding.activityWalkThroughTvSkip.setOnClickListener(this);
       activityWalkThroughBinding.activityWalkThroughTvStartNow.setOnClickListener(this);
        checkCameraPermission();

    }

    @Override
    protected void initToolbar() {

    }

    private void checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(WalkThroughActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(WalkThroughActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    1001);
        }
    }
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.activity_walk_through_tv_start_now:
                Utils.storeString(WalkThroughActivity.this,Constant.WALK_THROUGH,"done");
                Log.e("in walk Through", Constant.WALK_THROUGH);
                 intent = new Intent(WalkThroughActivity.this, LoginActivity.class);
                navigateToNextActivity(intent,true);
                break;
            case R.id.activity_walk_through_tv_skip:
                intent = new Intent(WalkThroughActivity.this, LoginActivity.class);
                navigateToNextActivity(intent,true);
                break;

        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1001: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
                return;
            }

        }
    }
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        activityWalkThroughBinding.activityWalkThroughTvHeading.setText(image_text_array_heading[position]);
        activityWalkThroughBinding.activityWalkThroughTvContent.setText(image_text_array_content[position]);
   }

    @Override
    public void onPageSelected(int position) {
    activityWalkThroughBinding.activityWalkThroughCp.setViewPager(activityWalkThroughBinding.activityWalkThroughVp,position);
        if(position==3){
            activityWalkThroughBinding.activityWalkThroughCp.setVisibility(View.GONE);
            activityWalkThroughBinding.activityWalkThroughTvStartNow.setVisibility(View.VISIBLE);
        }else{
            activityWalkThroughBinding.activityWalkThroughCp.setVisibility(View.VISIBLE);
            activityWalkThroughBinding.activityWalkThroughTvStartNow.setVisibility(View.GONE);
        }
    }


    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
