package com.recycletocoin.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.recycletocoin.R;
import com.recycletocoin.databinding.ActivitySuccessBinding;

public class SuccessActivity extends  BaseActivity implements View.OnClickListener {
    private ActivitySuccessBinding activitySuccessBinding;
    @Override
    protected void initView() {
        final String successMsg=getIntent().getStringExtra("successMessage");
        activitySuccessBinding = DataBindingUtil.setContentView(this,R.layout.activity_success);
        if(!TextUtils.isEmpty(successMsg)){
            activitySuccessBinding.activitySuccessTvSucessMsg.setText(successMsg);
        }
        }

    @Override
    protected void initToolbar() {
        Toolbar toolbar =(Toolbar)findViewById(R.id.activity_success_tb);
        toolbar.setBackgroundColor(getResources().getColor(R.color.purple));
        final ImageView logo=(ImageView)toolbar.findViewById(R.id.row_tb_iv_logo);
        final ImageView terms=(ImageView)toolbar.findViewById(R.id.row_tb_iv_terms);
        final ImageView help=(ImageView)toolbar.findViewById(R.id.row_tb_iv_help);
        final ImageView back=(ImageView)toolbar.findViewById(R.id.row_tb_iv_back);
        logo.setImageResource(R.drawable.ic_logo_white);
        back.setVisibility(View.VISIBLE);
        terms.setVisibility(View.GONE);
        help.setVisibility(View.GONE);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.row_tb_iv_back:
                onBackPressed();
                break;

        }

    }
}
