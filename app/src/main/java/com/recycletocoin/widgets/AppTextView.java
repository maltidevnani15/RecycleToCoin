package com.recycletocoin.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.recycletocoin.R;

public class AppTextView extends android.support.v7.widget.AppCompatTextView {
    public AppTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(isInEditMode())
            return;
        final TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.componentStyle);
        final String ttfName = ta.getText(0).toString();

        final Typeface font = Typeface.createFromAsset(context.getAssets(), ttfName);
        setTypeface(font);
        ta.recycle();
    }

}
