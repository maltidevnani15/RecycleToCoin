package com.recycletocoin.Model;


import com.google.gson.annotations.SerializedName;

public class TransactionModel {
    @SerializedName("trans_id")
    private String trans_id;
    @SerializedName("date_time")
    private String date_time;
    @SerializedName("points")
    private String points;
    @SerializedName("store_name")
    private String store_name;
    @SerializedName("qty")
    private String qty;
    @SerializedName("ether_addr")
    private String ether_addr;
    @SerializedName("tokens")
    private String token;

    @SerializedName("status")
    private String status;
    @SerializedName("type")
    private String type;

    public String getTrans_id() {
        return trans_id;
    }

    public void setTrans_id(String trans_id) {
        this.trans_id = trans_id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getEther_addr() {
        return ether_addr;
    }

    public void setEther_addr(String ether_addr) {
        this.ether_addr = ether_addr;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
