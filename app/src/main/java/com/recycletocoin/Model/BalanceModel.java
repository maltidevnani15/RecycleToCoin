package com.recycletocoin.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Malti on 9/30/2017.
 */

public class BalanceModel {
    public String getTotal_points() {
        return total_points;
    }

    public void setTotal_points(String total_points) {
        this.total_points = total_points;
    }

    public String getRedeemed_points() {
        return redeemed_points;
    }

    public void setRedeemed_points(String redeemed_points) {
        this.redeemed_points = redeemed_points;
    }

    public String getAvailable_points() {
        return available_points;
    }

    public void setAvailable_points(String available_points) {
        this.available_points = available_points;
    }

    @SerializedName("total_points")
    private String total_points;
    @SerializedName("redeemed_points")
    private String redeemed_points;
    @SerializedName("available_points")
    private String available_points;

}

