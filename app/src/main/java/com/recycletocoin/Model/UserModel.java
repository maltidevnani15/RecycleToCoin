package com.recycletocoin.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Malti on 1/14/2018.
 */

public class UserModel {
    @SerializedName("user_id")
    private String userId;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("email")
    private String email;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
