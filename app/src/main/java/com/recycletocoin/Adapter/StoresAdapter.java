package com.recycletocoin.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.recycletocoin.Interface.OnRecycleItemClick;
import com.recycletocoin.Model.StoreModel;
import com.recycletocoin.R;
import com.recycletocoin.databinding.RowStoresBinding;

import java.util.ArrayList;

public class StoresAdapter extends RecyclerView.Adapter<StoresAdapter.ViewHolder> {
    private RowStoresBinding rowMapAddressListAdapterBinding;
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<StoreModel>storeModelArrayList;
    private OnRecycleItemClick onRecycleItemClick;
     public StoresAdapter(final Context context,final ArrayList<StoreModel> storeModelArrayList){
        this.context=context;
        this.storeModelArrayList=storeModelArrayList;
    }
    @Override
    public StoresAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater=LayoutInflater.from(context);
        rowMapAddressListAdapterBinding= DataBindingUtil.inflate(inflater, R.layout.row_stores,parent,false);
        return new ViewHolder(rowMapAddressListAdapterBinding);
    }

    @Override
    public void onBindViewHolder(StoresAdapter.ViewHolder holder, int position) {
        holder.rowMapAddressListAdapterBinding.rowStoreTvCount.setText("Recycle shop " + String.format("%02d", position+1));
        holder.rowMapAddressListAdapterBinding.rowStoreTvDistance.setText("("+ String.format("%.2f", Float.parseFloat(storeModelArrayList.get(position).getDistance()))+" Km away"+ ")");
        holder.rowMapAddressListAdapterBinding.rowStoreTvAddress.setText(storeModelArrayList.get(position).getAddress());

    }

    @Override
    public int getItemCount() {
        return storeModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RowStoresBinding rowMapAddressListAdapterBinding;
        public ViewHolder(RowStoresBinding rowMapAddressListAdapterBinding) {
            super(rowMapAddressListAdapterBinding.getRoot());
            this.rowMapAddressListAdapterBinding=rowMapAddressListAdapterBinding;
            rowMapAddressListAdapterBinding.rowStoreTvGetDirection.setOnClickListener(this);
               }

        @Override
        public void onClick(View v) {
            if(onRecycleItemClick!=null){
                onRecycleItemClick.onItemClick(getAdapterPosition(),v);
            }
        }
    }
         public void setOnRecycleItemClick(OnRecycleItemClick onRecycleItemClick){
             this.onRecycleItemClick=onRecycleItemClick;
         }

}
