package com.recycletocoin.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.recycletocoin.Model.TransactionModel;
import com.recycletocoin.R;

import com.recycletocoin.databinding.RowTransactionBinding;

import java.util.ArrayList;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder>{
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<TransactionModel>transactionModelArrayList;
    private RowTransactionBinding rowTransactionAdapterBinding;
    public TransactionAdapter(final Context context,final ArrayList<TransactionModel>transactionModelArrayList){
        this.context=context;
        this.transactionModelArrayList=transactionModelArrayList;
    }
    @Override
    public TransactionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater=LayoutInflater.from(context);
        rowTransactionAdapterBinding= DataBindingUtil.inflate(inflater, R.layout.row_transaction,parent,false);
        return new ViewHolder(rowTransactionAdapterBinding);

    }

    @Override
    public void onBindViewHolder(TransactionAdapter.ViewHolder holder, int position) {
        holder.rowTransactionAdapterBinding.setTransactionData(transactionModelArrayList.get(position));
        if(transactionModelArrayList.get(position).getStatus().equalsIgnoreCase("1")){
            holder.rowTransactionAdapterBinding.activityTransactionIvStatus.setImageResource(R.drawable.ic_sucess_bt);
        }else if(transactionModelArrayList.get(position).getStatus().equalsIgnoreCase("2")){
            holder.rowTransactionAdapterBinding.activityTransactionIvStatus.setImageResource(R.drawable.ic_rejected_bt);

        }else if(transactionModelArrayList.get(position).getStatus().equalsIgnoreCase("0")){
            holder.rowTransactionAdapterBinding.activityTransactionIvStatus.setImageResource(R.drawable.ic_process_bt);

        }
    }

    @Override
    public int getItemCount() {
        return transactionModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RowTransactionBinding rowTransactionAdapterBinding;
        public ViewHolder(RowTransactionBinding rowTransactionAdapterBinding) {
            super(rowTransactionAdapterBinding.getRoot());
            this.rowTransactionAdapterBinding=rowTransactionAdapterBinding;
        }
    }
}
