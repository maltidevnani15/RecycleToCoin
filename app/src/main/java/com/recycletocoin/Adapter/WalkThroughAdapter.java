package com.recycletocoin.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.recycletocoin.R;

/**
 * Created by Malti on 9/17/2017.
 */

public class WalkThroughAdapter extends PagerAdapter{
    Context mContext;
    int imageArray[];

    public WalkThroughAdapter(Context context, int[] imageArray){
        this.mContext=context;
        this.imageArray=imageArray;
 }
    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater=(LayoutInflater)container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.layout_walk_through_adapter, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.walkThroughActivity_iv);
        imageView.setImageResource(imageArray[position]);
        container.addView(itemView);
          return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
