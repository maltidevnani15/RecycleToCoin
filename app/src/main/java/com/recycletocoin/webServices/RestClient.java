package com.recycletocoin.webServices;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.recycletocoin.BuildConfig;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private static RestClient instance;
    private ApiInterface apiInterface;

    public RestClient() {

        instance = this;
        final Gson gson = new GsonBuilder().registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .setLenient().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
        final OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        okHttpClientBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okHttpClientBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okHttpClientBuilder.readTimeout(60, TimeUnit.SECONDS);

      okHttpClientBuilder.addNetworkInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder();
                final String contentType = original.header("Content-Type");
                if (contentType!=null && !contentType.contains("multipart/form-data")) {
                    requestBuilder.removeHeader("Content-Type");
                    requestBuilder.addHeader("Content-Type", "application/x-www-form-urlencoded");
                }

                return chain.proceed(requestBuilder.build());
            }
        });

        final Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(ApiConstant.BASE_URL);

        retrofitBuilder.client(okHttpClientBuilder.build());
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create(gson));

        final Retrofit retrofit = retrofitBuilder.build();
        apiInterface = retrofit.create(ApiInterface.class);

    }

    public static RestClient getInstance() {
        return instance;
    }

    public ApiInterface getApiInterface() {
        return apiInterface;
    }
}
