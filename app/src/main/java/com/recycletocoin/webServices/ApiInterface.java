package com.recycletocoin.webServices;


import com.google.gson.JsonObject;
import com.recycletocoin.Model.BalanceModel;
import com.recycletocoin.Model.StoreModel;
import com.recycletocoin.Model.TransactionModel;
import com.recycletocoin.Model.UserModel;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("doUserSignUp")
    Call<JsonObject> signUp(@Field("full_name") String name,@Field("email") String email,@Field("password")String password);

    @FormUrlEncoded
    @POST("doUserLogin ")
    Call<UserModel>doLogin(@Field("email") String email, @Field("password")String password);

    @GET("getTotalClaimed ")
    Call<JsonObject>getclaimedToken();
    @FormUrlEncoded
    @POST("claim")
    Call<JsonObject> claimToken(@Field("code_string")String code, @Field("user_id") String userId);

    @FormUrlEncoded
    @POST("checkBalance")
    Call<BalanceModel> checkBalance(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("forgotPassword")
    Call<JsonObject> sendEmail(@Field("email") String email);


    @FormUrlEncoded
    @POST("getNearbyStops")
    Call<ArrayList<StoreModel>> getStores(@Field("latitude")String latitude,@Field("longitude")String longitude);

    @FormUrlEncoded
    @POST("getTransactions")
    Call<ArrayList<TransactionModel>>getTransaction(@Field("user_id")String user_id);
}